dcconf=--file docker/docker-compose.yml
tool=docker-compose $(dcconf) run --rm tool
db=docker-compose $(dcconf) run --rm db

start:
	@docker-compose $(dcconf) start

stop:
	@docker-compose $(dcconf) stop


up:
	@docker-compose $(dcconf) up -d

clean: stop
	@docker-compose $(dcconf) rm -f

console:
	$(tool) bash

composer.lock: composer.json
	composer self-update
	$(tool) bash -ci 'phpdismod -v ALL -s ALL xdebug && composer update --no-scripts --optimize-autoloader $(bundle)'
	#$(tool) bash -ci '/fixright && sudo -E -u phpuser php vendor/sensio/distribution-bundle/Resources/bin/build_bootstrap.php'
	$(tool) bash -ci 'chown -R $(stat -c "%u" /sources):$(stat -c "%g" /sources) /sources/vendor'

vendor: composer.lock
	composer self-update
	$(tool) bash -ci 'phpdismod -v ALL -s ALL xdebug && composer install --no-scripts --optimize-autoloader'
	#$(tool) bash -ci '/fixright && sudo -E -u phpuser php vendor/sensio/distribution-bundle/Resources/bin/build_bootstrap.php'
	$(tool) bash -ci 'chown -R $(stat -c "%u" /sources):$(stat -c "%g" /sources) /sources/vendor'
